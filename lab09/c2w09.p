set termina png
set output "2.png"


lgn( x, s ) = exp(-0.5*(log(x)/s)**2)/(x*s*sqrt(2*pi))
clgn( x, s ) = 0.5*(1+erf(log(x)/(s*sqrt(2))))

set multiplot layout 2,1
set bmargin 0 
set format x "" 
set ytics 0.2,0.2 
set key bottom
plot [0:2.5][0:1] clgn(x,0.5) t 's=0.5'
plot clgn(x,1) t 's=1.0', clgn(x,5) t 's=5.0'

set bmargin 
set tmargin 0 
set format x '%g'
set ytics 0,0.2,0.8
set key top
plot [0:2.5][0:1] lgn(x,0.5) t 's=0.5', lgn(x,1) t 's=1.0',
lgn(x,5) t 's=5.0'
unset multiplot
