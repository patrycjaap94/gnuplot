set encoding utf8
set terminal png
set output "zad20lab08.png"
set hidden3d
set isosamples 30
set palette model HSV functions gray,1,1
set pm3d at s
splot [-2:2][-2:2] exp(-(x**2 + y**2))*cos(x/4) *sin(y)*cos(2*(x**2+y**2))
test palette
