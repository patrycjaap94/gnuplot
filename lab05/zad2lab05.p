

set terminal png
set output "zad2lab05.png"

set multiplot layout 2,2
plot sin(x)
plot cos(x)
plot tan(x)
plot 1.0/tan(x)
unset multiplot


